#!/bin/bash
dt=`date +%F`
dm=`:`
log="/var/log/df/df_$dt"
cat /dev/null $log
df -h > /tmp/du$$

while read line
do
    fields=`echo $line | awk '{print NF}'`
    case $fields in
    5) echo $line | awk '{print $5,$dm,$2,$dm,$3}' >> $log;;
    6) echo $line | awk '{print $6,$dm,$2,$dm,$3}' >> $log;;
    esac
done < /tmp/du$$

rm /tmp/du$$
