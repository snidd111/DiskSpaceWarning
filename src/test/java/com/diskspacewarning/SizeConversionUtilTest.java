package com.diskspacewarning;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Brian Mancuso
 */
public class SizeConversionUtilTest {

    public SizeConversionUtilTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of toLong method, of class SizeConversionUtil.
     */
    @Test
    public void testToLong() {
        System.out.println("toLong");
        String s = "1024 MiB";
        long expResult = 1024L * 1024L * 1024L;
        long result = SizeConversionUtil.toLong(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toLong method, of class SizeConversionUtil.
     */
    @Test
    public void testToLongFraction() {
        System.out.println("toLongFraction");
        String s = "1024.6 MiB";
        long expResult = (long) (1024.6 * 1024.0 * 1024.0);
        long result = SizeConversionUtil.toLong(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toBytes method, of class SizeConversionUtil.
     */
    @Test
    public void testToBytes() {
        System.out.println("toBytes");
        String s = "1024 MiB";
        String expResult = (1024 * 1024 * 1024) + " B";
        String result = SizeConversionUtil.toBytes(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toKiB method, of class SizeConversionUtil.
     */
    @Test
    public void testToKiB() {
        System.out.println("toKiB");
        String s = "1024 MiB";
        String expResult = (1024 * 1024) + " KiB";
        String result = SizeConversionUtil.toKiB(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toMiB method, of class SizeConversionUtil.
     */
    @Test
    public void testToMiB() {
        System.out.println("toMiB");
        String s = "1073741824";
        String expResult = (1073741824 / 1024 / 1024) + " MiB";
        String result = SizeConversionUtil.toMiB(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toGiB method, of class SizeConversionUtil.
     */
    @Test
    public void testToGiB() {
        System.out.println("toGiB");
        String s = "1073741824";
        String expResult = (1073741824 / 1024/ 1024 / 1024) + " GiB";
        String result = SizeConversionUtil.toGiB(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toTiB method, of class SizeConversionUtil.
     */
    @Test
    public void testToTiB() {
        System.out.println("toTiB");
        String s = "1073741824 MiB";
        String expResult = (1073741824 / 1024/ 1024) + " TiB";
        String result = SizeConversionUtil.toTiB(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of toUnit method, of class SizeConversionUtil.
     */
    @Test
    public void testToUnit() {
        System.out.println("toUnit");
        String s = "2048 KiB";
        String unit = "MiB";
        String expResult = "2 MiB";
        String result = SizeConversionUtil.toUnit(s, unit);
        assertEquals(expResult, result);
    }

    /**
     * Test of autoConvert method, of class SizeConversionUtil.
     */
    @Test
    public void testAutoConvert() {
        System.out.println("autoConvert");
        String s = "1024 GiB";
        String expResult = "1 TiB";
        String result = SizeConversionUtil.autoConvert(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of autoConvert method, of class SizeConversionUtil.
     */
    @Test
    public void testAutoConvertTiB() {
        System.out.println("autoConvertTiB");
        String s = "1079 TiB";
        String expResult = "1079 TiB";
        String result = SizeConversionUtil.autoConvert(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of autoConvert method, of class SizeConversionUtil.
     */
    @Test
    public void testAutoConvertFraction1() {
        System.out.println("autoConvertFraction1");
        String s = "1079 GiB";
        String expResult = "1.1 TiB";
        String result = SizeConversionUtil.autoConvert(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of autoConvert method, of class SizeConversionUtil.
     */
    @Test
    public void testAutoConvertFraction2() {
        System.out.println("autoConvertFraction2");
        String s = ".5 GiB";
        String expResult = "512 MiB";
        String result = SizeConversionUtil.autoConvert(s);
        assertEquals(expResult, result);
    }

}
