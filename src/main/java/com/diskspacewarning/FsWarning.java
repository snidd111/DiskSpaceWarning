package com.diskspacewarning;

import java.time.LocalDateTime;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;

/**
 *
 * @author Brian Mancuso
 */
@Record(minOccurs = 0)
public class FsWarning {

    private LocalDateTime warningDate;
    private String hostname;
    private String filesystem;
    private int daysLeft;
    private String averageDailyConsumption;

    public FsWarning(LocalDateTime warningDate, String hostname, String filesystem, String averageDailyConsumption, int daysLeft) {
        this.warningDate = warningDate;
        this.hostname = hostname;
        this.filesystem = filesystem;
        this.daysLeft = daysLeft;
        this.averageDailyConsumption = averageDailyConsumption;
    }

    @Field(at = 0)
    public String getWarningDate() {
        return warningDate.toString();
    }

    public void setFormattedWarningDate(String warningDate) {
        this.warningDate = LocalDateTime.parse(warningDate);
    }

    public void setWarningDate(LocalDateTime warningDate) {
        this.warningDate = warningDate;
    }

    @Field(at = 1)
    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @Field(at = 2)
    public String getFilesystem() {
        return filesystem;
    }

    public void setFilesystem(String filesystem) {
        this.filesystem = filesystem;
    }

    @Field(at = 3)
    public String getAverageDailyConsumption() {
        return averageDailyConsumption;
    }

    public void setAverageDailyConsumption(String averageDailyConsumption) {
        this.averageDailyConsumption = averageDailyConsumption;
    }

    @Field(at = 4)
    public int getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft;
    }

}
