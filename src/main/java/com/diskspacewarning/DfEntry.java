package com.diskspacewarning;

import java.time.LocalDateTime;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;

/**
 *
 * @author Brian Mancuso
 */
@Record(minOccurs = 0)
public class DfEntry {

    private LocalDateTime dateTime;
    private String filesystem;
    private String size;
    private String used;

    public DfEntry() {

    }

    public DfEntry(LocalDateTime dateTime, String filesystem, String size, String used) {
        this.dateTime = dateTime;
        this.filesystem = filesystem;
        this.size = size;
        this.used = used;
    }

    @Field(at = 0)
    public String getFormattedDateTime() {
        return dateTime.toString();
    }

    public void setFormattedDateTime(String dateTime) {
        this.dateTime = LocalDateTime.parse(dateTime);
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Field(at = 1)
    public String getFilesystem() {
        return filesystem;
    }

    public void setFilesystem(String filesystem) {
        this.filesystem = filesystem;
    }

    @Field(at = 2)
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Field(at = 3)
    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

}
