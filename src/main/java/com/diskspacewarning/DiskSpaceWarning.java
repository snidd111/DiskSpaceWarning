package com.diskspacewarning;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;
import org.beanio.builder.DelimitedParserBuilder;
import org.beanio.builder.StreamBuilder;

/**
 *
 * @author Brian Mancuso
 */
public class DiskSpaceWarning {

    private static String LOG_FILE_DIR = "/var/log/df";
    private static String WARNING_FILE = "/var/disk_space_warning";
    private static int WARNING_WINDOW = 30; //Warning window in days
    private static String UNITS = "AUTO";
    private static char DELIMITER = ',';
    private static boolean PRINT_JSON = false;
    private static boolean PRINT_XML = false;
    private static boolean PRINT_CSV = false;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, Exception {
        processArgs(args);
        String hostname = InetAddress.getLocalHost().getHostName();
        File[] fileArray = populateLogFileArray(LOG_FILE_DIR);
        List<DfEntry> allDfEntries = new ArrayList<>();
        if (fileArray.length > 0) {
            StreamFactory sf = StreamFactory.newInstance();
            StreamBuilder builder = new StreamBuilder("dfentry").format("delimited").parser(new DelimitedParserBuilder(DELIMITER)).readOnly().addRecord(com.diskspacewarning.DfEntry.class);
            sf.define(builder);
            for (File f : fileArray) {
                allDfEntries.addAll(readLogFile(sf, f));
            }
        }
        Map<String, FsEntry> processedFsEntries = processDfEntries(allDfEntries);
        List<FsWarning> warnings = new ArrayList<>();
        LocalDateTime dt = LocalDateTime.now();
        for (String k : processedFsEntries.keySet()) {
            int daysRemaining = processedFsEntries.get(k).determineDaysLeftAtAverageGrowth();
            long growthRateInBytes = processedFsEntries.get(k).getAverageGrowthInBytes();
            String growthRate = SizeConversionUtil.toUnit(growthRateInBytes + "", UNITS);
            if (daysRemaining < WARNING_WINDOW && daysRemaining > -1) {
                warnings.add(new FsWarning(dt, hostname, k, growthRate, daysRemaining));
            }
        }
        if (!warnings.isEmpty()) {
            if (!PRINT_CSV && !PRINT_JSON && !PRINT_XML) {
                PRINT_CSV = true;
            }
            if (PRINT_CSV) {
                writeWarnings(WARNING_FILE + ".csv", warnings, "csv");
            }
            if (PRINT_JSON) {
                writeWarnings(WARNING_FILE + ".json", warnings, "json");
            }
            if (PRINT_XML) {
                writeWarnings(WARNING_FILE + ".xml", warnings, "xml");
            }
        }
    }

    private static File[] populateLogFileArray(String logDir) {
        File folder = new File(logDir);
        if (folder.isDirectory()) { //Return every file in the directory if a directory was passed
            return folder.listFiles();
        } else { //Otherwise, just use the file that was passed
            return new File[]{folder};
        }
    }

    private static List<DfEntry> readLogFile(StreamFactory sf, File logFile) {
        List<DfEntry> entries = new ArrayList<>();
        BeanReader in = sf.createReader("dfentry", logFile);
        DfEntry entry;
        while ((entry = (DfEntry) in.read()) != null) {
            entries.add(entry);
        }
        in.close();
        return entries;
    }

    private static Map<String, FsEntry> processDfEntries(List<DfEntry> allEntries) {
        Map<String, FsEntry> processedData = new HashMap<>();
        for (DfEntry d : allEntries) {
            if (processedData.containsKey(d.getFilesystem())) {
                FsEntry tmpFs = processedData.get(d.getFilesystem());
                if (tmpFs.getOldestDateTime().isAfter(d.getDateTime())) {
                    tmpFs.setOldestDateTime(d.getDateTime());
                    tmpFs.setOldestUsage(d.getUsed());
                } else if (tmpFs.getNewestDateTime().isBefore(d.getDateTime())) {
                    tmpFs.setNewestDateTime(d.getDateTime());
                    tmpFs.setNewestUsage(d.getUsed());
                    tmpFs.setSize(d.getSize()); //Note size is set on most recent entry since fs could have been increased
                }
                processedData.put(d.getFilesystem(), tmpFs);
            } else {
                FsEntry tmpFs = new FsEntry(d.getDateTime(), d.getUsed(), d.getDateTime(), d.getUsed(), d.getSize());
                processedData.put(d.getFilesystem(), tmpFs);
            }
        }
        return processedData;
    }

    private static void writeWarnings(String warningFlagFile, List<FsWarning> warnings, String format) {
        StreamFactory sf = StreamFactory.newInstance();
        StreamBuilder builder = new StreamBuilder("FsWarnings").format(format).writeOnly().addRecord(com.diskspacewarning.FsWarning.class);
        sf.define(builder);
        BeanWriter out = sf.createWriter("FsWarnings", new File(warningFlagFile));
        for (FsWarning warning : warnings) {
            out.write(warning);
        }
        out.flush();
        out.close();
    }

    private static void processArgs(String[] args) {
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].startsWith("-")) {
                    switch (args[i]) {
                        case "-c":
                        case "--csv":
                            PRINT_CSV = true;
                            break;
                        case "-d":
                        case "--days":
                            try {
                                WARNING_WINDOW = Integer.parseInt(args[i + 1]);
                            } catch (NumberFormatException e) {
                                System.out.println("Error! Couldn't parse number of days passed.");
                                System.exit(-1);
                            }
                            break;
                        case "--delimiter":
                            try {
                                DELIMITER = args[i + 1].charAt(0);
                            } catch (NumberFormatException e) {
                                System.out.println("Error! Couldn't parse delimiter passed.");
                                System.exit(-1);
                            }
                            break;
                        case "-h":
                        case "--help":
                            printHelp();
                            System.exit(0);
                            break;
                        case "-j":
                        case "--json":
                            PRINT_JSON = true;
                            break;
                        case "-l":
                        case "--logdir":
                            try {
                                LOG_FILE_DIR = args[i + 1];
                            } catch (Exception e) {
                                System.out.println("Error! Couldn't parse log file directory.");
                                System.exit(-1);
                            }
                            break;
                        case "-o":
                        case "--outdir":
                            try {
                                WARNING_FILE = args[i + 1];
                            } catch (Exception e) {
                                System.out.println("Error! Couldn't parse output directory.");
                                System.exit(-1);
                            }
                            break;
                        case "-u":
                        case "--units":
                            try {
                                switch (args[i + 1].toUpperCase()) {
                                    case "K":
                                    case "KB":
                                    case "KI":
                                    case "KIB":
                                        UNITS = "KiB";
                                        break;
                                    case "M":
                                    case "MB":
                                    case "MI":
                                    case "MIB":
                                        UNITS = "MiB";
                                        break;
                                    case "G":
                                    case "GB":
                                    case "GI":
                                    case "GIB":
                                        UNITS = "GiB";
                                        break;
                                    case "T":
                                    case "TB":
                                    case "TI":
                                    case "TIB":
                                        UNITS = "TiB";
                                        break;
                                    case "AUTO":
                                        UNITS = "AUTO";
                                        break;
                                    default:
                                        System.out.println("Unknown unit passed. Must select K, M, G,  T, or AUTO.");
                                        System.exit(-1);
                                }
                            } catch (Exception e) {
                                System.out.println("Error! Couldn't parse units to use.");
                                System.exit(-1);
                            }
                            break;
                        case "-x":
                        case "--xml":
                            PRINT_XML = true;
                            break;
                        default:
                            System.out.println("Error! Unknown parameter passed. See the help menu for more information.");
                            printHelp();
                            System.exit(-1);
                    }
                }
            }
        }
    }

    private static void printHelp() {
        System.out.println("Usage: DiskSpaceWarning [options]");
        System.out.println("  -c | --csv : Print the warnings in a csv formatted file. (default)");
        System.out.println("  -d | --days : The warning window to use. Defaults to 30 days.");
        System.out.println("  --delimiter : The delimiter used in the files to process. Must be a single character. Defaults to ','");
        System.out.println("  -j | --json : Print the warnings in a json formatted file.");
        System.out.println("  -h | --help : Print this help menu");
        System.out.println("  -l | --logdir : Where to find the files to process when determining the average consumption of data. Defaults to /var/log/df");
        System.out.println("  -o | --outdir : Where to put any determined warnings. Defaults to /var/disk_space_warning.csv");
        System.out.println("  -u | --units : What units to use when displaying the average storage consumption [K, M, G, T, or AUTO]. Defaults to AUTO");
        System.out.println("  -x | --xml : Print the warnings in an xml formatted file.");
    }
}
