package com.diskspacewarning;

import java.text.DecimalFormat;

/**
 *
 * @author Brian Mancuso
 */
public abstract class SizeConversionUtil {

    public static long toLong(String s) {
        s = s.toUpperCase();
        long inL = 0;
        if (s.contains("T")) {
            inL = (long) (Double.parseDouble(s.substring(0, s.indexOf("T")).trim()) * 1024 * 1024 * 1024 * 1024);
        } else if (s.contains("G")) {
            inL = (long) (Double.parseDouble(s.substring(0, s.indexOf("G")).trim()) * 1024 * 1024 * 1024);
        } else if (s.contains("M")) {
            inL = (long) (Double.parseDouble(s.substring(0, s.indexOf("M")).trim()) * 1024 * 1024);
        } else if (s.contains("K")) {
            inL = (long) (Double.parseDouble(s.substring(0, s.indexOf("K")).trim()) * 1024);
        } else if (s.matches("(?=[^A-Za-z]+$).*[0-9].*")) {  //assume bytes if there's no unit .
            inL = Long.parseLong(s);
        }
        return inL;
    }

    public static String toBytes(String s) {
        return toLong(s) + " B";
    }

    public static String toKiB(String s) {
        s = s.toUpperCase();
        double inKiB = 0;
        if (s.contains("T")) {
            inKiB = Double.parseDouble(s.substring(0, s.indexOf("T")).trim()) * 1024 * 1024 * 1024;
        } else if (s.contains("G")) {
            inKiB = Double.parseDouble(s.substring(0, s.indexOf("G")).trim()) * 1024 * 1024;
        } else if (s.contains("M")) {
            inKiB = Double.parseDouble(s.substring(0, s.indexOf("M")).trim()) * 1024;
        } else if (s.contains("K")) {
            inKiB = Double.parseDouble(s.substring(0, s.indexOf("K")).trim());
        } else if (s.matches("(?=[^A-Za-z]+$).*[0-9].*")) {  //assume bytes if there's no unit .
            inKiB = (Double.parseDouble(s)) / 1024;
        }

        return format(inKiB) + " KiB";
    }

    public static String toMiB(String s) {
        s = s.toUpperCase();
        double inMiB = 0;
        if (s.contains("T")) {
            inMiB = Double.parseDouble(s.substring(0, s.indexOf("T")).trim()) * 1024 * 1024;
        } else if (s.contains("G")) {
            inMiB = Double.parseDouble(s.substring(0, s.indexOf("G")).trim()) * 1024;
        } else if (s.contains("M")) {
            inMiB = Double.parseDouble(s.substring(0, s.indexOf("M")).trim());
        } else if (s.contains("K")) {
            inMiB = Double.parseDouble(s.substring(0, s.indexOf("K")).trim()) / 1024;
        } else if (s.matches("(?=[^A-Za-z]+$).*[0-9].*")) {  //assume bytes if there's no unit .
            inMiB = (Double.parseDouble(s)) / 1024 / 1024;
        }
        return format(inMiB) + " MiB";
    }

    public static String toGiB(String s) {
        s = s.toUpperCase();
        double inGiB = 0;
        if (s.contains("T")) {
            inGiB = Double.parseDouble(s.substring(0, s.indexOf("T")).trim()) * 1024;
        } else if (s.contains("G")) {
            inGiB = Double.parseDouble(s.substring(0, s.indexOf("G")).trim());
        } else if (s.contains("M")) {
            inGiB = Double.parseDouble(s.substring(0, s.indexOf("M")).trim()) / 1024;
        } else if (s.contains("K")) {
            inGiB = Double.parseDouble(s.substring(0, s.indexOf("K")).trim()) / 1024 / 1024;
        } else if (s.matches("(?=[^A-Za-z]+$).*[0-9].*")) {  //assume bytes if there's no unit .
            inGiB = (Double.parseDouble(s)) / 1024 / 1024 / 1024;
        }
        return format(inGiB) + " GiB";
    }

    public static String toTiB(String s) {
        s = s.toUpperCase();
        double inTiB = 0;
        if (s.contains("T")) {
            inTiB = Double.parseDouble(s.substring(0, s.indexOf("T")).trim());
        } else if (s.contains("G")) {
            inTiB = Double.parseDouble(s.substring(0, s.indexOf("G")).trim()) / 1024;
        } else if (s.contains("M")) {
            inTiB = Double.parseDouble(s.substring(0, s.indexOf("M")).trim()) / 1024 / 1024;
        } else if (s.contains("K")) {
            inTiB = Double.parseDouble(s.substring(0, s.indexOf("K")).trim()) / 1024 / 1024 / 1024;
        } else if (s.matches("(?=[^A-Za-z]+$).*[0-9].*")) {  //assume bytes if there's no unit .
            inTiB = (Double.parseDouble(s)) / 1024 / 1024 / 1024 / 1024;
        }
        return format(inTiB) + " TiB";
    }

    public static String toUnit(String s, String unit) {
        String toReturn = "";
        unit = unit.toUpperCase();
        switch (unit) {
            case "B":
            case "Bi":
                toReturn = toBytes(s);
                break;
            case "K":
            case "KB":
            case "KI":
            case "KIB":
                toReturn = toKiB(s);
                break;
            case "M":
            case "MB":
            case "MI":
            case "MIB":
                toReturn = toMiB(s);
                break;
            case "G":
            case "GB":
            case "GI":
            case "GIB":
                toReturn = toGiB(s);
                break;
            case "T":
            case "TB":
            case "TI":
            case "TIB":
                toReturn = toTiB(s);
                break;
            case "AUTO":
                toReturn = autoConvert(s);
                break;
            default:
                System.out.println("Unknown unit passed. Must select B, K, M, G, T, or AUTO");
                System.exit(-1);
        }
        return toReturn;
    }

    public static String autoConvert(String s) {
        long bytes = toLong(s);
        if (bytes <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KiB", "MiB", "GiB", "TiB"};
        int digitGroups = (int) (Math.log10(bytes) / Math.log10(1024));
        if (digitGroups > units.length - 1) {
            digitGroups = units.length - 1;
        }
        return format(bytes / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private static String format(double value) {
        return new DecimalFormat("###0.#").format(value);
    }

}
