package com.diskspacewarning;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author Brian Mancuso
 */
public class FsEntry {

    private LocalDateTime oldestDateTime;
    private String oldestUsage;
    private LocalDateTime newestDateTime;
    private String newestUsage;
    private String size;

    public FsEntry(LocalDateTime oldestDateTime, String oldestUsage, LocalDateTime newestDateTime, String newestUsage, String size) {
        this.oldestDateTime = oldestDateTime;
        this.oldestUsage = oldestUsage;
        this.newestDateTime = newestDateTime;
        this.newestUsage = newestUsage;
        this.size = size;
    }

    public LocalDateTime getOldestDateTime() {
        return oldestDateTime;
    }

    public void setOldestDateTime(LocalDateTime oldestDateTime) {
        this.oldestDateTime = oldestDateTime;
    }

    public String getOldestUsage() {
        return oldestUsage;
    }

    public void setOldestUsage(String oldestUsage) {
        this.oldestUsage = oldestUsage;
    }

    public LocalDateTime getNewestDateTime() {
        return newestDateTime;
    }

    public void setNewestDateTime(LocalDateTime newestDateTime) {
        this.newestDateTime = newestDateTime;
    }

    public String getNewestUsage() {
        return newestUsage;
    }

    public void setNewestUsage(String newestUsage) {
        this.newestUsage = newestUsage;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int determineDaysLeftAtAverageGrowth() throws Exception {
        double averageGrowthInB = getAverageGrowthInBytes();
        long sizeInL = SizeConversionUtil.toLong(size);
        long newUsageInL = SizeConversionUtil.toLong(newestUsage);
        if (averageGrowthInB > 0) {
            return (int) ((sizeInL - newUsageInL) / averageGrowthInB);
        }
        return -1;
    }

    public long getAverageGrowthInBytes() throws Exception {
        if (newestDateTime == null || newestUsage == null || oldestDateTime == null || oldestUsage == null) {
            throw new Exception("Must have already setup newest and oldest dates and usages");
        }
        if (oldestUsage.equals(newestUsage) || oldestDateTime.equals(newestDateTime)) {
            return -1; //Space or date hasn't changed
        }
        long days = ChronoUnit.DAYS.between(oldestDateTime, newestDateTime);
        long newUsageInL = SizeConversionUtil.toLong(newestUsage);
        long oldUsageInL = SizeConversionUtil.toLong(oldestUsage);
        long result = (newUsageInL - oldUsageInL) / days;
        return result;
    }

}
